﻿USE [SVC_ORDER];

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Order]') AND type in (N'U'))

  BEGIN

    CREATE TABLE [dbo].[Order](
      [Id] [uniqueidentifier] NOT NULL,
      [UserId] [uniqueidentifier] NOT NULL,
      [UserName] [varchar](100) NOT NULL,
      [Amount] [decimal](9, 2) NOT NULL,
      [DateCreated] [datetime] NOT NULL,
      [DateUpdated] [datetime] NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY])

    ALTER TABLE [dbo].[Order] ADD DEFAULT (newid()) FOR [Id]

  END