﻿USE [SVC_USER];

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND type in (N'U'))

  BEGIN

    CREATE TABLE [dbo].[User](
      [Id] [uniqueidentifier] NOT NULL,
      [Name] [varchar](100) NOT NULL,
      [DateCreated] [datetime] NOT NULL,
      [DateUpdated] [datetime] NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY])

    ALTER TABLE [dbo].[User] ADD  DEFAULT (newid()) FOR [Id]

  END