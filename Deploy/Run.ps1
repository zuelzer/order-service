docker run --name sql_developer_linux -p 1433:1433 -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=yourStrong(!)Password" -d mcr.microsoft.com/mssql/server:2017-latest
docker run --name seq -p 5341:80 -e "ACCEPT_EULA=Y" -d datalust/seq:latest

Start-Sleep -s 5

$sqlDirectory = "SQLScripts"
$containerSqlServerName = "sql_developer_linux"
$userSa = "sa"
$password = "yourStrong(!)Password"

Start-Sleep -s 5

# Create Sql Server temporary directory
docker exec $containerSqlServerName mkdir -p /tmp/sqlserver/scripts
docker exec $containerSqlServerName bash -c 'rm -rf /tmp/sqlserver/scripts/*.*'

Start-Sleep -s 10

Get-ChildItem $sqlDirectory -Recurse -Filter *.sql | 
Foreach-Object {
    $filename = $_.Name

    Write-Host "Copying filename " $_.FullName " to '${containerSqlServerName}:/tmp/sqlserver/scripts/$filename'".
    docker cp $_.FullName ${containerSqlServerName}:"/tmp/sqlserver/scripts/$filename"

    Write-Host "Executing sqcmd with filname: " $filename
    #execute script in sql server in docker
    docker exec -it $containerSqlServerName /opt/mssql-tools/bin/sqlcmd -S localhost -U $userSa -P $password -i "/tmp/sqlserver/scripts/$filename"
}