# Order Service

## Description

The project propose is delivery a order-service to perform order transactions connected to another service dependence (user-service).

The order-service was created with microservice concepts using the following Microsoft .NET Core stack:

- .NET Core 3.1
- SQL Server 2017
- EntityFrameworkCore
- Docker
- Swagger UI
- HealtCheck UI
- Monitoring and Logging

## Instalation

Please follow the steps below to install the order-service on your local environment:

### Software Pre-requirements

- Windows 10
- Visual Studio 2019 Community (or latest)
- Docker Desktop (Linux containers)
- CommandLine and Powershell

### Project Source Code

The order-service source code is [hosted on GitLab.com](https://gitlab.com/zuelzer/order-service.git), please clone the project for your local environment.

### Installing Project Pre-requirements

After cloning the project: 

- Find the "Deploy" directory into the source project
- Using the Powershell command line, to run the "Run.ps1" file.

The installation file will perform the following tasks:

- Create and initialize a Docker container for Datalust Seq (Log Server)
- Create and initialize a Docker container for SQL Server 2017
- Execute the initials SQL commands to create the project database

The process can probably take some time, as you will need to download the Docker images.

If you get some SQL error during the processes, please run (by Powershell) the "Run.ps1" file once again.

### Starting Order Service

- Find the deploy "OrderService\src\OrderService" directory into the source project
- Using the Visual Studio, to open the "OrderService.sln" solution file.
- Set project "Presentation.WebAPI" as Startup Project
- And run the application on IIS Express mode

Is supposed the order-service starting on port 8282, but you can change it in the project properties (launchSettings.json) if needed.

### Starting User Service

- Find the deploy "OrderService\src\UserService" directory into the source project
- Using the Visual Studio, to open the "UserService.sln" solution file.
- Set project "Presentation.WebAPI" as Startup Project
- And run the application on IIS Express mode

Is supposed the user-service starting on port 8181, please keep this port, because it is already configurated in order-service.

## Features (First MVP)

The following interfaces are available:

### Swagger - Order Service

[Swagger UI](https://localhost:8282/swagger) - started by default

Through the Swagger UI, you can perform payment requests also read the documentation schema for request and response types.

**GET** `​/Order​/v1​/Order - Get list order`

**POST** `​/Order​/v1​/Order - Create an order`

### HealtCheck - Order Service

[HealtCheck](https://localhost:8282/healthcheck) - REST api to check service readiness probes.

[HealtCheck UI](https://localhost:8282/healthchecks-ui) - Web interface to check the health of the service and its dependencies.

[HealtCheck Api](https://localhost:8282/healthchecks-api) - REST Api to check the health of the service and its dependencies.

### Swagger - User service

[Swagger UI](https://localhost:8181/swagger) - started by default

Through the Swagger UI, you can perform payment requests also read the documentation schema for request and response types.

**GET** `​/User​/v1​/User/{userId} - Get user details`

**POST** `​​/User​/v1​/User - Create a user`

**PUT** `​​/User​/v1​/User - Update a user`

**DELETE** `​/User​/v1​/User/{userId} - Delete a user`

### HealtCheck - User service

[HealtCheck](https://localhost:8181/healthcheck) - REST api to check service readiness probes.

[HealtCheck UI](https://localhost:8181/healthchecks-ui) - Web interface to check the health of the service and its dependencies.

[HealtCheck Api](https://localhost:8181/healthchecks-api) - REST Api to check the health of the service and its dependencies.

### Monitoring and Logging

The payment-gateway-service provides many ways to monitoring the application by logging, see below:

[Datalust Seq](http://localhost:5341/): Web interface to analyse and monitoring the application through the application and HTTP logs.

## Backlog and Technical Debt

When defining an initial scope for the project, it was necessary to define the delivery priorities covered in the first MVP.

Now we have a backlog product with new features and technical debits that I will describe below:

### Resilience

Add resilience to infrastructure and services dependencies avoiding unavailabilities:

- SQL Server
- User Service

### Testing

- Add Performace Tests
- Add Integration Tests
- Improve Unit Tests to coverage 100% of code, currently the code coverage is 65%

### Deployment

- Add Pipelines CI/CD to build the application and run tests
- Add order-service running on Docker
- Add user-service running on Docker
- Add code first strategy with the database layer
- Remove SQL Scripts deployment strategy

### API and Security

- Add authentication on order-service
- Add authentication on user-service
- Add authentication on Swagger
- Add authentication between services transactions
- Add data encryption between services transactions