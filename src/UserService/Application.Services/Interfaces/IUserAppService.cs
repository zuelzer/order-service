﻿namespace Checkout.UserService.Application.Services.Interfaces
{
    using Checkout.UserService.Application.DTO.Response;
    using System;
    using System.Threading.Tasks;

    public interface IUserAppService
    {
        Task<User> GetUserAsync(Guid userId);

        Task<User> CreateUserAsync(DTO.Request.User user);

        Task<User> UpdateUserAsync(Guid userId, DTO.Request.User user);

        Task DeleteUserAsync(Guid userId);
    }
}
