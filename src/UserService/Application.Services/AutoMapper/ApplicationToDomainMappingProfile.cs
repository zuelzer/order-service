﻿namespace Checkout.UserService.Application.Services.AutoMapper
{
    using Checkout.UserService.Application.DTO.Request;
    using global::AutoMapper;

    public class ApplicationToDomainMappingProfile : Profile
    {
        public ApplicationToDomainMappingProfile()
        {
            CreateMap<User, Domain.Model.Entities.User>();
        }
    }
}
