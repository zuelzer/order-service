﻿namespace Checkout.UserService.Application.Services.AutoMapper
{
    using Checkout.UserService.Application.DTO.Response;
    using global::AutoMapper;

    public class DomainToApplicationMappingProfile : Profile
    {
        public DomainToApplicationMappingProfile()
        {
            CreateMap<Domain.Model.Entities.User, User>();
        }
    }
}
