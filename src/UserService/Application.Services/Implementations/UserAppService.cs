﻿namespace Checkout.UserService.Application.Services.Implementations
{
    using Checkout.UserService.Application.DTO.Response;
    using Checkout.UserService.Application.Services.Interfaces;
    using Checkout.UserService.Domain.Services.Interfaces;
    using Checkout.UserService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.UserService.Infrastructure.CrossCutting.Extensions;
    using global::AutoMapper;
    using System;
    using System.Threading.Tasks;

    public class UserAppService : IUserAppService
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UserAppService(IMapper mapper, IUserService userService)
        {
            this._mapper = mapper;
            this._userService = userService;
        }

        public async Task<User> GetUserAsync(Guid userId)
        {
            var domainUser = await this._userService.GetUserAsync(userId).ConfigureAwait(false);

            Intercept.Against<ResourceNotFoundException>(domainUser.IsNull(), "User not found");

            return this._mapper.Map<Domain.Model.Entities.User, User>(domainUser);
        }

        public async Task<User> CreateUserAsync(DTO.Request.User user)
        {
            Intercept.Against<InvalidDomainEntityException>(user.IsNull(), "User cannot be null");

            var domainUser = this._mapper.Map<DTO.Request.User, Domain.Model.Entities.User>(user);

            domainUser = await this._userService.CreateUserAsync(domainUser).ConfigureAwait(false);

            return this._mapper.Map<Domain.Model.Entities.User, User>(domainUser);
        }

        public async Task<User> UpdateUserAsync(Guid userId, DTO.Request.User user)
        {
            Intercept.Against<InvalidDomainEntityException>(user.IsNull(), "User cannot be null");

            var domainUser = await this._userService.GetUserAsync(userId).ConfigureAwait(false);

            if (domainUser.IsNotNull())
            {
                domainUser.SetName(user.Name);
            }

            domainUser = await this._userService.UpdateUserAsync(domainUser).ConfigureAwait(false);

            return this._mapper.Map<Domain.Model.Entities.User, User>(domainUser);
        }

        public async Task DeleteUserAsync(Guid userId)
        {
            await this._userService.DeleteUserAsync(userId).ConfigureAwait(false);
        }
    }
}
