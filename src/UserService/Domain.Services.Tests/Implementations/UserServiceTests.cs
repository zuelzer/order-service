﻿namespace Checkout.UserService.Domain.Services.Tests.Implementations
{
    using Checkout.UserService.Domain.Core.Repositories;
    using Checkout.UserService.Domain.Model.Entities;
    using Checkout.UserService.Domain.Services.Implementations;
    using Checkout.UserService.Domain.Services.Interfaces;
    using Checkout.UserService.Infrastructure.CrossCutting.Exceptions;
    using Moq;
    using System;
    using Xunit;

    [Trait("Category", "Domain.Services")]
    public class UserServiceTests
    {
        private readonly Mock<IUserRepository> _userRepositoryMock;

        public UserServiceTests()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
        }

        [Fact]
        public async void CreateUserAsync_NullUser_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            User user = null;
            var expectedMessage = "User cannot be null";

            // Act
            var service = this.CreateUserService();
            var exception = await Assert.ThrowsAsync<InvalidDomainEntityException>(() => service.CreateUserAsync(user));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void CreateUserAsync_AlreadyExistsUser_ThrowsResourceAlreadyExistsException()
        {
            // Arrange
            var expectedUserName = "Name test";
            var expectedMessage = "User already exists";
            User modelUser = new User(expectedUserName);

            this._userRepositoryMock.Setup(m => m.GetAsync(It.IsAny<Guid>())).ReturnsAsync(modelUser);

            // Act
            var service = this.CreateUserService();
            var exception = await Assert.ThrowsAsync<ResourceAlreadyExistsException>(() => service.CreateUserAsync(modelUser));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void UpdateUserAsync_NullUser_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            User user = null;
            var expectedMessage = "User cannot be null";

            // Act
            var service = this.CreateUserService();
            var exception = await Assert.ThrowsAsync<InvalidDomainEntityException>(() => service.UpdateUserAsync(user));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void UpdateUserAsync_InvalidUser_ThrowsResourceNotFoundException()
        {
            // Arrange
            var expectedUserName = "Name test";
            var expectedMessage = "User not found";
            User modelUser = new User(expectedUserName);

            // Act
            var service = this.CreateUserService();
            var exception = await Assert.ThrowsAsync<ResourceNotFoundException>(() => service.UpdateUserAsync(modelUser));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void UpdateUserAsync_InvalidUserId_ThrowsResourceNotFoundException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var expectedMessage = "User not found";

            // Act
            var service = this.CreateUserService();
            var exception = await Assert.ThrowsAsync<ResourceNotFoundException>(() => service.DeleteUserAsync(userId));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        private IUserService CreateUserService()
        {
            return new UserService(this._userRepositoryMock.Object);
        }
    }
}
