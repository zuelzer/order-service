﻿namespace Checkout.UserService.Application.Services.Tests.Implementations
{
    using Checkout.UserService.Application.DTO.Request;
    using Checkout.UserService.Application.Services.AutoMapper;
    using Checkout.UserService.Application.Services.Implementations;
    using Checkout.UserService.Application.Services.Interfaces;
    using Checkout.UserService.Domain.Services.Interfaces;
    using Checkout.UserService.Infrastructure.CrossCutting.Exceptions;
    using global::AutoMapper;
    using Microsoft.Extensions.Logging;
    using Moq;
    using System;
    using Xunit;

    [Trait("Category", "Application.Services")]
    public class UserAppServiceTests
    {
        private readonly Mock<ILoggerFactory> _loggerFactoryMock;
        private readonly Mock<IUserService> _userServiceMock;

        public UserAppServiceTests()
        {
            _loggerFactoryMock = new Mock<ILoggerFactory>();
            _userServiceMock = new Mock<IUserService>();
        }

        [Fact]
        public async void GetUserAsync_InvalidUserId_ThrowsResourceNotFoundException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var expectedMessage = "User not found";

            // Act
            var service = this.CreateUserAppService();
            var exception = await Assert.ThrowsAsync<ResourceNotFoundException>(() => service.GetUserAsync(userId));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void GetUserAsync_ValidUserId_ReturnsFilledUser()
        {
            // Arrange
            var expectedUserName = "Name test";
            var modelUser = new Domain.Model.Entities.User(expectedUserName);

            this._userServiceMock.Setup(m => m.GetUserAsync(It.IsAny<Guid>())).ReturnsAsync(modelUser);

            // Act
            var service = this.CreateUserAppService();
            var filledUser = await service.GetUserAsync(modelUser.Id);

            // Assert
            Assert.NotNull(filledUser);
            Assert.Equal(expectedUserName, filledUser.Name);
        }

        [Fact]
        public async void CreateUserAsync_NullUser_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            User user = null;
            var expectedMessage = "User cannot be null";

            // Act
            var service = this.CreateUserAppService();
            var exception = await Assert.ThrowsAsync<InvalidDomainEntityException>(() => service.CreateUserAsync(user));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void CreateUserAsync_ValidUser_ReturnsFilledUser()
        {
            // Arrange
            var expectedUserName = "Name test";
            var requestUser = new Application.DTO.Request.User { Name = expectedUserName };
            var modelUser = new Domain.Model.Entities.User(expectedUserName);

            this._userServiceMock.Setup(m => m.CreateUserAsync(It.IsAny<Domain.Model.Entities.User>())).ReturnsAsync(modelUser);

            // Act
            var service = this.CreateUserAppService();
            var filledUser = await service.CreateUserAsync(requestUser);

            // Assert
            Assert.NotNull(filledUser);
            Assert.Equal(expectedUserName, filledUser.Name);
        }

        [Fact]
        public async void UpdateUserAsync_NullUser_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            User user = null;
            var userId = Guid.NewGuid();
            var expectedMessage = "User cannot be null";

            // Act
            var service = this.CreateUserAppService();
            var exception = await Assert.ThrowsAsync<InvalidDomainEntityException>(() => service.UpdateUserAsync(userId, user));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void UpdateUserAsync_ValidUser_ReturnsFilledUser()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var expectedUserName = "Name test";
            var requestUser = new Application.DTO.Request.User { Name = expectedUserName };
            var modelUser = new Domain.Model.Entities.User(expectedUserName);

            this._userServiceMock.Setup(m => m.UpdateUserAsync(It.IsAny<Domain.Model.Entities.User>())).ReturnsAsync(modelUser);

            // Act
            var service = this.CreateUserAppService();
            var filledUser = await service.UpdateUserAsync(userId, requestUser);

            // Assert
            Assert.NotNull(filledUser);
            Assert.Equal(expectedUserName, filledUser.Name);
        }

        private IUserAppService CreateUserAppService()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ApplicationToDomainMappingProfile());
                cfg.AddProfile(new DomainToApplicationMappingProfile());
            });

            var _mapper = mapperConfiguration.CreateMapper();

            return new UserAppService(_mapper, this._userServiceMock.Object);
        }
    }
}
