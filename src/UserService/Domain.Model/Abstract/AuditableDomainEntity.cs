﻿namespace Checkout.UserService.Domain.Model.Abstract
{
    using System;
    using Checkout.UserService.Domain.Model.Interfaces;

    public abstract class AuditableDomainEntity : DomainEntity, IAuditableEntity
    {
        protected AuditableDomainEntity()
        {
            this.CreatedOn = DateTime.UtcNow;
        }

        public DateTime CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
