﻿namespace Checkout.UserService.Domain.Model.Abstract
{
    using System;
    using Checkout.UserService.Infrastructure.CrossCutting.Extensions;

    public abstract class DomainEntity
    {
        protected DomainEntity()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public static bool operator ==(DomainEntity left, DomainEntity right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }

            if (((object)left).IsNull() || ((object)right).IsNull())
            {
                return false;
            }

            if (left.Id == right.Id)
            {
                return true;
            }

            return false;
        }

        public static bool operator !=(DomainEntity left, DomainEntity right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            return (obj as DomainEntity) == this;
        }

        public override int GetHashCode() => unchecked((17 * 23) + this.Id.GetHashCode());
    }
}
