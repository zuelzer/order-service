﻿namespace Checkout.UserService.Domain.Model.Entities
{
    using Checkout.UserService.Domain.Model.Abstract;
    using Checkout.UserService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.UserService.Infrastructure.CrossCutting.Extensions;

    public class User : AuditableDomainEntity
    {
        protected User() { }

        public User(string name)
        {
            this.Name = name;
        }

        public string Name { get; private set; }

        public void SetName(string name)
        {
            this.Name = name;
        }

        public void Validate()
        {
            Intercept.Against<InvalidDomainEntityException>(this.Name.IsNull(), "The name cannot be null");
            Intercept.Against<InvalidDomainEntityException>(this.Name.Length > 100, "The name cannot be longer than a hundred charactes");
        }
    }
}
