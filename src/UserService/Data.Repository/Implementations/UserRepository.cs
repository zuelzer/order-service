﻿namespace Checkout.UserService.Data.Repository.Implementations
{
    using global::AutoMapper;
    using Checkout.UserService.Data.Repository.Context;
    using Checkout.UserService.Data.Repository.Entities;
    using Checkout.UserService.Domain.Core.Repositories;
    using Checkout.UserService.Infrastructure.CrossCutting.Extensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    public class UserRepository : IUserRepository
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly Func<SqlServerContext> _factory;

        public UserRepository(ILoggerFactory loggerFactory, IMapper mapper, Func<SqlServerContext> factory)
        {
            this._logger = loggerFactory.CreateLogger<UserRepository>();
            this._mapper = mapper;
            this._factory = factory;
        }

        public async Task<Domain.Model.Entities.User> GetAsync(Guid userId)
        {
            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    var dataUser = await dbContext.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == userId).ConfigureAwait(false);

                    if (dataUser.IsNotNull())
                    {
                        return this._mapper.Map<User, Domain.Model.Entities.User>(dataUser);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unable to get a transaction (GetUser) from database: $userId: {userId}");
            }

            return null;
        }

        public async Task<Domain.Model.Entities.User> InsertAsync(Domain.Model.Entities.User user)
        {
            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    var dataUser = this._mapper.Map<Domain.Model.Entities.User, User>(user);

                    dbContext.Add(dataUser);

                    await dbContext.SaveChangesAsync().ConfigureAwait(false);

                    return this._mapper.Map<User, Domain.Model.Entities.User>(dataUser);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unable to save a transaction (InsertUser) into the database: $id: {user.Id} - $name: {user.Name}");
            }

            return null;
        }

        public async Task<Domain.Model.Entities.User> UpdateAsync(Domain.Model.Entities.User user)
        {
            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    var dataUser = this._mapper.Map<Domain.Model.Entities.User, User>(user);

                    dbContext.Update(dataUser);

                    await dbContext.SaveChangesAsync().ConfigureAwait(false);

                    return this._mapper.Map<User, Domain.Model.Entities.User>(dataUser);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unable to save a transaction (UpdateUser) into the database: $id: {user.Id} - $name: {user.Name}");
            }

            return null;
        }

        public async Task DeleteAsync(Domain.Model.Entities.User user)
        {
            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    var dataUser = this._mapper.Map<Domain.Model.Entities.User, User>(user);

                    dbContext.Remove(dataUser);

                    await dbContext.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unable to save a transaction (DeleteUser) into the database: $id: {user.Id} - $name: {user.Name}");
            }
        }
    }
}