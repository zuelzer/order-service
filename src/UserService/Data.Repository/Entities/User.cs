﻿namespace Checkout.UserService.Data.Repository.Entities
{
    using System;

    public class User
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }
    }
}
