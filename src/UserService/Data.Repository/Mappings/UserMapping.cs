﻿namespace Checkout.UserService.Data.Repository.Mappings
{
    using Checkout.UserService.Data.Repository.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(t => t.Id);

            builder.Property(t => t.Name).HasColumnName("Name").HasMaxLength(100).IsRequired();
            builder.Property(t => t.DateCreated).HasColumnName("DateCreated").IsRequired();
            builder.Property(t => t.DateUpdated).HasColumnName("DateUpdated");

            builder.ToTable("User");
        }
    }
}