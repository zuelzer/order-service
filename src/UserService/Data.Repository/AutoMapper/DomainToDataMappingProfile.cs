﻿namespace Checkout.UserService.Data.Repository.AutoMapper
{
    using Checkout.UserService.Data.Repository.Entities;
    using global::AutoMapper;

    public class DomainToDataMappingProfile : Profile
    {
        public DomainToDataMappingProfile()
        {
            CreateMap<Domain.Model.Entities.User, User>()
                .ForMember(s => s.Name, d => d.MapFrom(t => t.Name))
                .ForMember(s => s.DateCreated, d => d.MapFrom(t => t.CreatedOn))
                .ForMember(s => s.DateUpdated, d => d.MapFrom(t => t.UpdatedOn));
        }
    }
}
