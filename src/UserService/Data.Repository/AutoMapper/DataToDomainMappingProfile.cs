﻿namespace Checkout.UserService.Data.Repository.AutoMapper
{
    using global::AutoMapper;
    using Checkout.UserService.Data.Repository.Entities;

    public class DataToDomainMappingProfile : Profile
    {
        public DataToDomainMappingProfile()
        {
            CreateMap<User, Domain.Model.Entities.User>()
                .ForMember(s => s.Name, d => d.MapFrom(t => t.Name))
                .ForMember(s => s.CreatedOn, d => d.MapFrom(t => t.DateCreated))
                .ForMember(s => s.UpdatedOn, d => d.MapFrom(t => t.DateUpdated));
        }
    }
}
