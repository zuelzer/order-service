﻿namespace Checkout.UserService.Domain.Model.Entities.Tests
{
    using Checkout.UserService.Domain.Model.Entities;
    using Checkout.UserService.Infrastructure.CrossCutting.Exceptions;
    using Xunit;

    [Trait("Category", "Domain.Model")]
    public class UserTests
    {
        [Fact]
        public void Constructor_ValidUser_NewUserWithFilledProperties()
        {
            // Arrange
            var name = "UserName";

            // Act
            var user = new User(name);

            // Assert
            Assert.NotNull(user);
            Assert.Equal(name, user.Name);
        }

        [Fact]
        public void SetName_ValidName_NamePropertyFilled()
        {
            // Arrange
            var name = "UserName";

            // Act
            var user = new User(null);
            user.SetName(name);

            // Assert
            Assert.NotNull(user);
            Assert.Equal(name, user.Name);
        }

        [Fact]
        public void Validate_NullName_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var expectedMessage = "The name cannot be null";

            // Act
            var user = new User(null);
            var exception = Assert.Throws<InvalidDomainEntityException>(() => user.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void Validate_NameLongerThanAHundredCharacters_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var name = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tortor sed augue tortor.";
            var expectedMessage = "The name cannot be longer than a hundred charactes";

            // Act
            var user = new User(name);
            var exception = Assert.Throws<InvalidDomainEntityException>(() => user.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }
    }
}
