﻿namespace Checkout.UserService.Domain.Services.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Checkout.UserService.Domain.Model.Entities;

    public interface IUserService
    {
        Task<User> GetUserAsync(Guid userId);

        Task<User> CreateUserAsync(User user);

        Task<User> UpdateUserAsync(User user);

        Task DeleteUserAsync(Guid userId);
    }
}
