﻿namespace Checkout.UserService.Domain.Services.Implementations
{
    using System;
    using System.Threading.Tasks;
    using Checkout.UserService.Domain.Core.Repositories;
    using Checkout.UserService.Domain.Model.Entities;
    using Checkout.UserService.Domain.Services.Interfaces;
    using Checkout.UserService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.UserService.Infrastructure.CrossCutting.Extensions;

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public async Task<User> GetUserAsync(Guid userId)
        {
            return await this._userRepository.GetAsync(userId).ConfigureAwait(false);
        }

        public async Task<User> CreateUserAsync(User user)
        {
            Intercept.Against<InvalidDomainEntityException>(user.IsNull(), "User cannot be null");
            
            var userAlreadyExists = await this._userRepository.GetAsync(user.Id).ConfigureAwait(false);
            Intercept.Against<ResourceAlreadyExistsException>(userAlreadyExists.IsNotNull(), "User already exists");

            user.Validate();

            return await this._userRepository.InsertAsync(user).ConfigureAwait(false);
        }

        public async Task<User> UpdateUserAsync(User user)
        {
            Intercept.Against<InvalidDomainEntityException>(user.IsNull(), "User cannot be null");

            var userAlreadyExists = await this._userRepository.GetAsync(user.Id).ConfigureAwait(false);
            Intercept.Against<ResourceNotFoundException>(userAlreadyExists.IsNull(), "User not found");

            user.Validate();

            return await this._userRepository.UpdateAsync(user).ConfigureAwait(false);
        }

        public async Task DeleteUserAsync(Guid userId)
        {
            var user = await this._userRepository.GetAsync(userId).ConfigureAwait(false);

            Intercept.Against<ResourceNotFoundException>(user.IsNull(), "User not found");

            await this._userRepository.DeleteAsync(user).ConfigureAwait(false);
        }
    }
}
