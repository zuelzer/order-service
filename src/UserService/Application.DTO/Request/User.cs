﻿namespace Checkout.UserService.Application.DTO.Request
{
    using System;

    public class User
    {
        public string Name { get; set; }
    }
}
