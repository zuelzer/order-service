﻿namespace Checkout.UserService.Application.DTO.Response
{
    using System;

    public class User
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}
