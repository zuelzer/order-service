﻿namespace Checkout.UserService.Presentation.Web.Controllers
{
    using Checkout.UserService.Application.DTO.Request;
    using Checkout.UserService.Application.Services.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Net;
    using System.Threading.Tasks;

    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserAppService _userAppService;

        public UserController(IUserAppService userAppService)
        {
            this._userAppService = userAppService;
        }

        /// <summary>
        /// Get user details
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("User/{userId:guid}", Name = "GetUser")]
        [ProducesResponseType(typeof(Application.DTO.Response.User), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetUserAsync([FromRoute] Guid userId)
        {
            var result = await this._userAppService.GetUserAsync(userId).ConfigureAwait(false);

            return this.Ok(result);
        }

        /// <summary>
        /// Create a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("User", Name = "CreateUser")]
        [ProducesResponseType(typeof(Application.DTO.Response.User), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateUserAsync([FromBody] User user)
        {
            var result = await this._userAppService.CreateUserAsync(user).ConfigureAwait(false);

            return this.CreatedAtRoute("GetUser", new { userId = result.Id }, result);
        }

        /// <summary>
        /// Update a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("User/{userId:guid}", Name = "UpdateUser")]
        [ProducesResponseType(typeof(Application.DTO.Response.User), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateUserAsync([FromRoute] Guid userId, [FromBody] User user)
        {
            var result = await this._userAppService.UpdateUserAsync(userId, user).ConfigureAwait(false);

            return this.Ok(result);
        }

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("User/{userId:guid}", Name = "DeleteUserAsync")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> DeleteUserAsync([FromRoute] Guid userId)
        {
            await this._userAppService.DeleteUserAsync(userId).ConfigureAwait(false);

            return this.NoContent();
        }
    }
}
