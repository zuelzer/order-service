﻿namespace Checkout.UserService.Presentation.Bootstrapper
{
    using Checkout.UserService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class AppSettingsBootstrapper
    {
        public static void RegisterServices(IServiceCollection services,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddSingleton<EntityFrameworkConfiguration>(entityFrameworkConfiguration);
            services.AddSingleton<HealthCheckConfiguration>(healthCheckConfiguration);
            services.AddSingleton<LoggingConfiguration>(loggingConfiguration);
            services.AddSingleton<SwaggerConfiguration>(swaggerConfiguration);
        }
    }
}
