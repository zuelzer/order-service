﻿namespace Checkout.UserService.Presentation.Bootstrapper
{
    using System;
    using Checkout.UserService.Data.Repository.Context;
    using Checkout.UserService.Data.Repository.Implementations;
    using Checkout.UserService.Domain.Core.Repositories;
    using Checkout.UserService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class DataBootstrapper
    {
        public static void RegisterServices(IServiceCollection services, EntityFrameworkConfiguration entityFrameworkConfiguration)
        {
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddDbContext<SqlServerContext>();
            services.AddTransient<Func<SqlServerContext>>((provider) => new Func<SqlServerContext>(() => new SqlServerContext(entityFrameworkConfiguration)));
        }
    }
}
