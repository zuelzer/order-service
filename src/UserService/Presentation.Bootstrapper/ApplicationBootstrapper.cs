﻿namespace Checkout.UserService.Presentation.Bootstrapper
{
    using Checkout.UserService.Application.Services.Implementations;
    using Checkout.UserService.Application.Services.Interfaces;
    using Microsoft.Extensions.DependencyInjection;

    public class ApplicationBootstrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IUserAppService, UserAppService>();
        }
    }
}
