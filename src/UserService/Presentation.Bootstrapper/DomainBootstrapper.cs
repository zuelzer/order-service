﻿namespace Checkout.UserService.Presentation.Bootstrapper
{
    using AutoMapper;
    using Checkout.UserService.Domain.Services.Implementations;
    using Checkout.UserService.Domain.Services.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    
    public class DomainBootstrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));
            services.AddScoped<IUserService, UserService>();
        }
    }
}
