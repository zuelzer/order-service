﻿namespace Checkout.UserService.Presentation.Bootstrapper
{
    using AutoMapper;
    using Checkout.UserService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class NativeInjectorBootstrapper
    {
        public static void RegisterServices(IServiceCollection services,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddScoped(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));

            AppSettingsBootstrapper.RegisterServices(services, entityFrameworkConfiguration, healthCheckConfiguration, loggingConfiguration, swaggerConfiguration);
            ApplicationBootstrapper.RegisterServices(services);
            DomainBootstrapper.RegisterServices(services);
            DataBootstrapper.RegisterServices(services, entityFrameworkConfiguration);
        }
    }
}
