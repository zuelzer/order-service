﻿namespace Checkout.UserService.Domain.Core.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Checkout.UserService.Domain.Model.Entities;

    public interface IUserRepository
    {
        Task<User> GetAsync(Guid userId);

        Task<User> InsertAsync(User user);

        Task<User> UpdateAsync(User user);

        Task DeleteAsync(User user);
    }
}
