﻿namespace Checkout.UserService.Infrastructure.CrossCutting.Configuration
{
    public class LoggingConfiguration
    {
        public string ApplicationInsightUrl { get; set; }
    }
}
