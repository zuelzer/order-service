﻿namespace Checkout.UserService.Infrastructure.CrossCutting.Configuration
{
    public class HealthCheckConfiguration
    {
        public string DefaultPath { get; set; }

        public string UIPath { get; set; }

        public string ApiPath { get; set; }
    }
}
