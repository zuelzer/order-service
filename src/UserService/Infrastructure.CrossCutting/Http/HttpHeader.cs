﻿namespace Checkout.UserService.Infrastructure.CrossCutting.Http
{
    public class HttpHeader
    {
        public HttpHeader(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        public string Name { get; }

        public string Value { get; }
    }
}
