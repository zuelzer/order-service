﻿namespace Checkout.UserService.Infrastructure.CrossCutting.Serializer
{
    public interface IStringSerializer
    {
        string SerializeToString(object item);
    }
}
