﻿namespace Checkout.OrderService.Application.Services.Tests.Implementations
{
    using Checkout.OrderService.Application.DTO.Request;
    using Checkout.OrderService.Application.Services.AutoMapper;
    using Checkout.OrderService.Application.Services.Implementations;
    using Checkout.OrderService.Application.Services.Interfaces;
    using Checkout.OrderService.Data.Gateway.Interfaces;
    using Checkout.OrderService.Data.Gateway.UserGateway.Entities;
    using Checkout.OrderService.Domain.Services.Interfaces;
    using Checkout.OrderService.Infrastructure.CrossCutting.Exceptions;
    using global::AutoMapper;
    using Moq;
    using System;
    using Xunit;

    [Trait("Category", "Application.Services")]
    public class OrderAppServiceTests
    {
        private readonly Mock<IOrderService> _orderServiceMock;
        private readonly Mock<IUserGateway> _userGatewayMock;

        public OrderAppServiceTests()
        {
            _orderServiceMock = new Mock<IOrderService>();
            _userGatewayMock = new Mock<IUserGateway>();
        }

        [Fact]
        public async void CreateOrderAsync_NullOrder_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            Order order = null;
            var expectedMessage = "Order cannot be null";

            // Act
            var service = this.CreateOrderAppService();
            var exception = await Assert.ThrowsAsync<InvalidDomainEntityException>(() => service.CreateOrderAsync(order));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void CreateOrderAsync_InvalidUserId_ThrowsResourceNotFoundException()
        {
            // Arrange
            Order order = new Order
            {
                UserId = Guid.NewGuid(),
                Amount = 10
            };

            var expectedMessage = "User not found";

            // Act
            var service = this.CreateOrderAppService();
            var exception = await Assert.ThrowsAsync<ResourceNotFoundException>(() => service.CreateOrderAsync(order));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async void CreateOrderAsync_ValidOrder_ReturnsAllFilledProperties()
        {
            // Arrange
            var order = new Order
            {
                UserId = Guid.NewGuid(),
                Amount = 10
            };

            var userResponse = new UserResponse
            {
                Id = order.UserId,
                Name = "UserName"
            };

            var domainOrder = new Domain.Model.Entities.Order(order.UserId, order.Amount);
            domainOrder.SetUserName(userResponse.Name);

            this._orderServiceMock.Setup(m => m.CreateOrderAsync(It.IsAny<Domain.Model.Entities.Order>())).ReturnsAsync(domainOrder);
            this._userGatewayMock.Setup(m => m.GetUserAsync(It.IsAny<Guid>())).ReturnsAsync(userResponse);

            // Act
            var service = this.CreateOrderAppService();
            var fullOrder = await service.CreateOrderAsync(order).ConfigureAwait(false);

            // Assert
            Assert.NotNull(fullOrder);
            Assert.Equal(fullOrder.UserId, userResponse.Id);
            Assert.Equal(fullOrder.UserName, userResponse.Name);
        }

        private IOrderAppService CreateOrderAppService()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ApplicationToDomainMappingProfile());
                cfg.AddProfile(new DomainToApplicationMappingProfile());
            });

            var _mapper = mapperConfiguration.CreateMapper();

            return new OrderAppService(_mapper, this._orderServiceMock.Object, this._userGatewayMock.Object);
        }
    }
}
