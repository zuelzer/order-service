﻿namespace Checkout.OrderService.Domain.Model.Interfaces
{
    using System;

    public interface IAuditableEntity
    {
        DateTime CreatedOn { get; set; }

        DateTime? UpdatedOn { get; set; }
    }
}
