﻿namespace Checkout.OrderService.Domain.Model.Abstract
{
    using Checkout.OrderService.Domain.Model.Interfaces;
    using System;

    public abstract class AuditableDomainEntity : DomainEntity, IAuditableEntity
    {
        protected AuditableDomainEntity()
        {
            this.CreatedOn = DateTime.UtcNow;
        }

        public DateTime CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
