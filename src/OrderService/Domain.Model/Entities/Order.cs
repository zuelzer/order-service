﻿namespace Checkout.OrderService.Domain.Model.Entities
{
    using Checkout.OrderService.Domain.Model.Abstract;
    using Checkout.OrderService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.OrderService.Infrastructure.CrossCutting.Extensions;
    using System;

    public class Order : AuditableDomainEntity
    {
        public Order(Guid userId, decimal amount)
        {
            this.UserId = userId;
            this.Amount = amount;
        }

        public Guid UserId { get; private set; }

        public string UserName { get; private set; }

        public decimal Amount { get; private set; }

        public void SetUserName(string userName)
        {
            this.UserName = userName;
        }

        public void Validate()
        {
            Intercept.Against<InvalidDomainEntityException>(this.UserId == default(Guid), "User id cannot be null");
            Intercept.Against<InvalidDomainEntityException>(this.UserName.IsNull(), "User name cannot be null");
            Intercept.Against<InvalidDomainEntityException>(this.UserName.Length > 100, "User name cannot be longer than a hundred charactes");
            Intercept.Against<InvalidDomainEntityException>(this.Amount <= 0, "Amount must be greater than zero");
        }
    }
}
