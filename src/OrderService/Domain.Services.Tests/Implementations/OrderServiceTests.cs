﻿namespace Checkout.OrderService.Domain.Services.Tests.Implementations
{
    using Checkout.OrderService.Domain.Core.Repositories;
    using Checkout.OrderService.Domain.Model.Entities;
    using Checkout.OrderService.Domain.Services.Implementations;
    using Checkout.OrderService.Domain.Services.Interfaces;
    using Checkout.OrderService.Infrastructure.CrossCutting.Exceptions;
    using Moq;
    using System;
    using Xunit;

    [Trait("Category", "Domain.Services")]
    public class  OrderServiceTests
    {
        private readonly Mock<IOrderRepository> _orderRepositoryMock;

        public OrderServiceTests()
        {
            _orderRepositoryMock = new Mock<IOrderRepository>();
        }

        [Fact]
        public async void CreateOrderAsync_NullOrder_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            Order order = null;
            var expectedMessage = "Order cannot be null";

            // Act
            var service = this.CreateOrderService();
            var exception = await Assert.ThrowsAsync<InvalidDomainEntityException>(() => service.CreateOrderAsync(order));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        private IOrderService CreateOrderService()
        {
            return new OrderService(this._orderRepositoryMock.Object);
        }
    }
}
