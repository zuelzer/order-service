﻿namespace Checkout.OrderService.Infrastructure.CrossCutting.Configuration
{
    public class UserServiceConfiguration
    {
        public string BaseUrl { get; set; }
        public string GetUserEndpointUrl { get; set; }
    }
}
