﻿namespace Checkout.OrderService.Infrastructure.CrossCutting.Configuration
{
    public class LoggingConfiguration
    {
        public string ApplicationInsightUrl { get; set; }
    }
}
