﻿namespace Checkout.OrderService.Infrastructure.CrossCutting.Serializer
{
    public interface IStringSerializer
    {
        string SerializeToString(object item);
    }
}
