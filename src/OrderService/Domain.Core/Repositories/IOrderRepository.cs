﻿namespace Checkout.OrderService.Domain.Core.Repositories
{
    using Checkout.OrderService.Domain.Model.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IOrderRepository
    {
        Task<List<Order>> GetListGroupedByUserAsync();

        Task<Order> InsertAsync(Order order);
    }
}
