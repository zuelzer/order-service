﻿namespace Checkout.OrderService.Presentation.Web.Controllers
{
    using Checkout.OrderService.Application.DTO.Request;
    using Checkout.OrderService.Application.Services.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using System.Net;
    using System.Threading.Tasks;

    public class OrderController : ControllerBase
    {
        private readonly IOrderAppService _orderAppService;

        public OrderController(IOrderAppService orderAppService)
        {
            this._orderAppService = orderAppService;
        }

        /// <summary>
        /// Get list order
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Order", Name = "GetListOrder")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetListOrderAsync()
        {
            var result = await this._orderAppService.GetListOrderGroupedByUserAsync().ConfigureAwait(false);

            return this.Ok(result);
        }

        /// <summary>
        /// Create an order
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Order", Name = "CreateOrder")]
        [ProducesResponseType(typeof(Application.DTO.Response.FullOrder), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateOrderAsync([FromBody] Order order)
        {
            var result = await this._orderAppService.CreateOrderAsync(order).ConfigureAwait(false);

            return this.CreatedAtRoute("CreateOrder", new { orderId = result.Id }, result);
        }
    }
}
