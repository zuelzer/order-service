﻿namespace Checkout.OrderService.Presentation.Bootstrapper
{
    using Checkout.OrderService.Application.Services.Implementations;
    using Checkout.OrderService.Application.Services.Interfaces;
    using Microsoft.Extensions.DependencyInjection;

    public class ApplicationBootstrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IOrderAppService, OrderAppService>();
        }
    }
}
