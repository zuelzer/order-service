﻿namespace Checkout.OrderService.Presentation.Bootstrapper
{
    using Checkout.OrderService.Data.Repository.Context;
    using Checkout.OrderService.Data.Repository.Implementations;
    using Checkout.OrderService.Domain.Core.Repositories;
    using Checkout.OrderService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using System;

    public class DataBootstrapper
    {
        public static void RegisterServices(IServiceCollection services, EntityFrameworkConfiguration entityFrameworkConfiguration)
        {
            services.AddScoped<IOrderRepository, OrderRepository>();

            services.AddDbContext<SqlServerContext>();
            services.AddTransient<Func<SqlServerContext>>((provider) => new Func<SqlServerContext>(() => new SqlServerContext(entityFrameworkConfiguration)));
        }
    }
}
