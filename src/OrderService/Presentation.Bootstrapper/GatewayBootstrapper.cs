﻿namespace Checkout.OrderService.Presentation.Bootstrapper
{
    using Checkout.OrderService.Data.Gateway.Interfaces;
    using Checkout.OrderService.Data.Gateway.UserGateway.Implementations;
    using Microsoft.Extensions.DependencyInjection;

    public class GatewayBootstrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IUserGateway, UserGateway>();
        }
    }
}
