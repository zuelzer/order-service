﻿namespace Checkout.OrderService.Presentation.Bootstrapper
{
    using Checkout.OrderService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class AppSettingsBootstrapper
    {
        public static void RegisterServices(IServiceCollection services,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            UserServiceConfiguration userServiceConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddSingleton<EntityFrameworkConfiguration>(entityFrameworkConfiguration);
            services.AddSingleton<UserServiceConfiguration>(userServiceConfiguration);
            services.AddSingleton<HealthCheckConfiguration>(healthCheckConfiguration);
            services.AddSingleton<LoggingConfiguration>(loggingConfiguration);
            services.AddSingleton<SwaggerConfiguration>(swaggerConfiguration);
        }
    }
}
