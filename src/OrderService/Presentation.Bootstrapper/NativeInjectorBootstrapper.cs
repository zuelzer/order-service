﻿namespace Checkout.OrderService.Presentation.Bootstrapper
{
    using AutoMapper;
    using Checkout.OrderService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class NativeInjectorBootstrapper
    {
        public static void RegisterServices(IServiceCollection services,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            UserServiceConfiguration userServiceConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddScoped(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));

            AppSettingsBootstrapper.RegisterServices(services, entityFrameworkConfiguration, userServiceConfiguration, healthCheckConfiguration, loggingConfiguration, swaggerConfiguration);
            ApplicationBootstrapper.RegisterServices(services);
            DomainBootstrapper.RegisterServices(services);
            DataBootstrapper.RegisterServices(services, entityFrameworkConfiguration);
            GatewayBootstrapper.RegisterServices(services);
        }
    }
}
