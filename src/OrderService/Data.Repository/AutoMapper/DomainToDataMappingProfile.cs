﻿namespace Checkout.OrderService.Data.Repository.AutoMapper
{
    using Checkout.OrderService.Data.Repository.Entities;
    using global::AutoMapper;

    public class DomainToDataMappingProfile : Profile
    {
        public DomainToDataMappingProfile()
        {
            CreateMap<Domain.Model.Entities.Order, Order>()
                .ForMember(s => s.DateCreated, d => d.MapFrom(t => t.CreatedOn))
                .ForMember(s => s.DateUpdated, d => d.MapFrom(t => t.UpdatedOn));
        }
    }
}
