﻿namespace Checkout.OrderService.Data.Repository.AutoMapper
{
    using Checkout.OrderService.Data.Repository.Entities;
    using global::AutoMapper;

    public class DataToDomainMappingProfile : Profile
    {
        public DataToDomainMappingProfile()
        {
            CreateMap<Order, Domain.Model.Entities.Order>()
                .ForMember(s => s.CreatedOn, d => d.MapFrom(t => t.DateCreated))
                .ForMember(s => s.UpdatedOn, d => d.MapFrom(t => t.DateUpdated));
        }
    }
}
