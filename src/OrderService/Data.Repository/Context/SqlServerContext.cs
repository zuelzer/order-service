﻿namespace Checkout.OrderService.Data.Repository.Context
{
    using Checkout.OrderService.Data.Repository.Entities;
    using Checkout.OrderService.Data.Repository.Mappings;
    using Checkout.OrderService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;
    using System.Linq;

    public class SqlServerContext : DbContext
    {
        private readonly EntityFrameworkConfiguration _entityFrameworkConfiguration;

        public SqlServerContext(EntityFrameworkConfiguration entityFrameworkConfiguration)
            : base()
        {
            this._entityFrameworkConfiguration = entityFrameworkConfiguration;
        }

        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.RegisterConventions(modelBuilder);
            this.AddEntityConfigurations(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(this._entityFrameworkConfiguration.ToString());
            }
        }

        private void RegisterConventions(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(string)))
            {
                property.AsProperty().Builder.IsUnicode(false, ConfigurationSource.Convention);
            }
        }

        private void AddEntityConfigurations(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OrderMapping());
        }
    }
}
