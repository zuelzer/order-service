﻿namespace Checkout.OrderService.Data.Repository.Implementations
{
    using Checkout.OrderService.Data.Repository.Context;
    using Checkout.OrderService.Data.Repository.Entities;
    using Checkout.OrderService.Domain.Core.Repositories;
    using global::AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class OrderRepository : IOrderRepository
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly Func<SqlServerContext> _factory;

        public OrderRepository(ILoggerFactory loggerFactory, IMapper mapper, Func<SqlServerContext> factory)
        {
            this._logger = loggerFactory.CreateLogger<OrderRepository>();
            this._mapper = mapper;
            this._factory = factory;
        }

        public async Task<List<Domain.Model.Entities.Order>> GetListGroupedByUserAsync()
        {
            var orders = new List<Domain.Model.Entities.Order>();

            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    //var dataOrders = await dbContext.Orders.ToListAsync().ConfigureAwait(false);
                    var dataOrders = await dbContext.Orders
                        .GroupBy(o => new { o.UserId, o.UserName })
                        .Select(go => new Order
                        {
                            UserId = go.Key.UserId,
                            UserName = go.Key.UserName,
                            Amount = go.Sum(g => g.Amount)
                        })
                        .ToListAsync()
                        .ConfigureAwait(false);

                    foreach (var dataOrder in dataOrders)
                    {
                        orders.Add(this._mapper.Map<Order, Domain.Model.Entities.Order>(dataOrder));
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unable to get a transaction (GetListGroupedByUser) from database");
            }

            return orders;
        }

        public async Task<Domain.Model.Entities.Order> InsertAsync(Domain.Model.Entities.Order order)
        {
            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    var dataOrder = this._mapper.Map<Domain.Model.Entities.Order, Order>(order);

                    dbContext.Add(dataOrder);

                    await dbContext.SaveChangesAsync().ConfigureAwait(false);

                    return this._mapper.Map<Order, Domain.Model.Entities.Order>(dataOrder);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unable to save a transaction (InsertOrder) into the database: $id: {order.Id} - $userId: {order.UserId} - $userName: {order.UserName} - $amount: {order.Amount}");
            }

            return null;
        }
    }
}
