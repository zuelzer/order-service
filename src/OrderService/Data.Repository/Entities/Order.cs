﻿namespace Checkout.OrderService.Data.Repository.Entities
{
    using System;

    public class Order
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public decimal Amount { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }
    }
}
