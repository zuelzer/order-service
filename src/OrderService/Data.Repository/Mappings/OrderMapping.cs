﻿namespace Checkout.OrderService.Data.Repository.Mappings
{
    using Checkout.OrderService.Data.Repository.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class OrderMapping : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.UserId).HasColumnName("UserId").IsRequired();
            builder.Property(o => o.UserName).HasColumnName("UserName").HasMaxLength(100).IsRequired();
            builder.Property(o => o.Amount).HasColumnName("Amount").IsRequired();
            builder.Property(o => o.DateCreated).HasColumnName("DateCreated").IsRequired();
            builder.Property(o => o.DateUpdated).HasColumnName("DateUpdated");

            builder.ToTable("Order");
        }
    }
}
