﻿namespace Checkout.OrderService.Domain.Services.Implementations
{
    using Checkout.OrderService.Domain.Core.Repositories;
    using Checkout.OrderService.Domain.Model.Entities;
    using Checkout.OrderService.Domain.Services.Interfaces;
    using Checkout.OrderService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.OrderService.Infrastructure.CrossCutting.Extensions;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            this._orderRepository = orderRepository;
        }

        public async Task<List<Order>> GetListOrderGroupedByUserAsync()
        {
            return await this._orderRepository.GetListGroupedByUserAsync().ConfigureAwait(false);
        }

        public async Task<Order> CreateOrderAsync(Order order)
        {
            Intercept.Against<InvalidDomainEntityException>(order.IsNull(), "Order cannot be null");

            order.Validate();

            return await this._orderRepository.InsertAsync(order).ConfigureAwait(false);
        }
    }
}
