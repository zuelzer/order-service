﻿namespace Checkout.OrderService.Domain.Services.Interfaces
{
    using Checkout.OrderService.Domain.Model.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IOrderService
    {
        Task<List<Order>> GetListOrderGroupedByUserAsync();

        Task<Order> CreateOrderAsync(Order order);
    }
}
