﻿namespace Checkout.OrderService.Application.DTO.Response
{
    using System;

    public class FullOrder
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}
