﻿namespace Checkout.OrderService.Application.DTO.Response
{
    using System;

    public class Order
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public decimal Amount { get; set; }
    }
}
