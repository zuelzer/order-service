﻿namespace Checkout.OrderService.Application.DTO.Request
{
    using System;

    public class Order
    {
        public Guid UserId { get; set; }

        public decimal Amount { get; set; }
    }
}
