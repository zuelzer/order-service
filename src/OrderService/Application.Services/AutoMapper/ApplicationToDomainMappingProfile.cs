﻿namespace Checkout.OrderService.Application.Services.AutoMapper
{
    using Checkout.OrderService.Application.DTO.Request;
    using global::AutoMapper;

    public class ApplicationToDomainMappingProfile : Profile
    {
        public ApplicationToDomainMappingProfile()
        {
            CreateMap<Order, Domain.Model.Entities.Order>();
        }
    }
}
