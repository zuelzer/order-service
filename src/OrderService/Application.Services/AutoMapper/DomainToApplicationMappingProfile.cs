﻿namespace Checkout.OrderService.Application.Services.AutoMapper
{
    using Checkout.OrderService.Application.DTO.Response;
    using global::AutoMapper;

    public class DomainToApplicationMappingProfile : Profile
    {
        public DomainToApplicationMappingProfile()
        {
            CreateMap<Domain.Model.Entities.Order, Order>();
            CreateMap<Domain.Model.Entities.Order, FullOrder>();
        }
    }
}
