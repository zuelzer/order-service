﻿namespace Checkout.OrderService.Application.Services.Implementations
{
    using Checkout.OrderService.Application.DTO.Response;
    using Checkout.OrderService.Application.Services.Interfaces;
    using Checkout.OrderService.Data.Gateway.Interfaces;
    using Checkout.OrderService.Domain.Services.Interfaces;
    using Checkout.OrderService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.OrderService.Infrastructure.CrossCutting.Extensions;
    using global::AutoMapper;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class OrderAppService : IOrderAppService
    {
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;
        private readonly IUserGateway _userGateway;

        public OrderAppService(IMapper mapper, IOrderService orderService, IUserGateway userGateway)
        {
            this._mapper = mapper;
            this._orderService = orderService;
            this._userGateway = userGateway;
        }

        public async Task<List<Order>> GetListOrderGroupedByUserAsync()
        {
            var orders = new List<Order>();

            var domainOrders = await _orderService.GetListOrderGroupedByUserAsync().ConfigureAwait(false);

            foreach (var order in domainOrders)
            {
                orders.Add(this._mapper.Map<Domain.Model.Entities.Order, Order>(order));
            }

            return orders;
        }

        public async Task<FullOrder> CreateOrderAsync(DTO.Request.Order order)
        {
            Intercept.Against<InvalidDomainEntityException>(order.IsNull(), "Order cannot be null");

            var userGateway = await _userGateway.GetUserAsync(order.UserId).ConfigureAwait(false);
            Intercept.Against<ResourceNotFoundException>(userGateway.IsNull(), "User not found");

            var domainOrder = this._mapper.Map<DTO.Request.Order, Domain.Model.Entities.Order>(order);
            domainOrder.SetUserName(userGateway.Name);

            domainOrder = await this._orderService.CreateOrderAsync(domainOrder).ConfigureAwait(false);

            return this._mapper.Map<Domain.Model.Entities.Order, FullOrder>(domainOrder);
        }
    }
}
