﻿namespace Checkout.OrderService.Application.Services.Interfaces
{
    using Checkout.OrderService.Application.DTO.Response;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IOrderAppService
    {
        Task<List<Order>> GetListOrderGroupedByUserAsync();

        Task<FullOrder> CreateOrderAsync(DTO.Request.Order order);
    }
}
