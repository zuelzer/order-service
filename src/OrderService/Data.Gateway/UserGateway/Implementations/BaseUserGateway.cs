﻿namespace Checkout.OrderService.Data.Gateway.UserGateway.Implementations
{
    using Checkout.OrderService.Infrastructure.CrossCutting.Configuration;
    using Checkout.OrderService.Infrastructure.CrossCutting.Http;
    using System;
    using System.Net.Http;

    public class BaseUserGateway
    {
        private const string userAgent = "order-service/v1";

        protected readonly Uri _baseAddress;
        protected readonly UserServiceConfiguration _userServiceConfiguration;

        protected IHttpConnection HttpConnection { get; private set; }

        protected HttpHeader[] HttpHeaders { get; private set; }

        protected BaseUserGateway(UserServiceConfiguration userServiceConfiguration)
        {
            this._userServiceConfiguration = userServiceConfiguration;
            this._baseAddress = new Uri(this._userServiceConfiguration.BaseUrl);

            var httpClient = new HttpClient
            {
                BaseAddress = _baseAddress
            };

            this.HttpConnection = new HttpClientConnection(httpClient, userAgent);
        }
    }
}
