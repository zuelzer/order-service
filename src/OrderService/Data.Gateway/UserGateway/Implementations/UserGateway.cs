﻿namespace Checkout.OrderService.Data.Gateway.UserGateway.Implementations
{
    using Checkout.OrderService.Data.Gateway.Interfaces;
    using Checkout.OrderService.Data.Gateway.UserGateway.Entities;
    using Checkout.OrderService.Infrastructure.CrossCutting.Configuration;
    using Checkout.OrderService.Infrastructure.CrossCutting.Extensions;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    public class UserGateway : BaseUserGateway, IUserGateway
    {
        private readonly ILogger _logger;

        public UserGateway(ILoggerFactory loggerFactory, UserServiceConfiguration userServiceConfiguration)
            : base(userServiceConfiguration)
        {
            this._logger = loggerFactory.CreateLogger<UserGateway>();
        }

        public async Task<UserResponse> GetUserAsync(Guid userId)
        {
            var getUserEndpointUrl = new Uri(base._baseAddress, $"{base._userServiceConfiguration.GetUserEndpointUrl}/{userId}");

            try
            {
                var userGateway = await HttpConnection.GetAsync<UserResponse>(getUserEndpointUrl, base.HttpHeaders).ConfigureAwait(false);

                return userGateway.IsNull() || userGateway.Id == default(Guid) ? null : userGateway;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to perform get user: {url}", getUserEndpointUrl.AbsolutePath);
            }

            return null;
        }
    }
}
