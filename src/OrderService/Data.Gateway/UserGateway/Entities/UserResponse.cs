﻿namespace Checkout.OrderService.Data.Gateway.UserGateway.Entities
{
    using System;

    public class UserResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}
