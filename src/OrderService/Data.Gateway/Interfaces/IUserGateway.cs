﻿namespace Checkout.OrderService.Data.Gateway.Interfaces
{
    using Checkout.OrderService.Data.Gateway.UserGateway.Entities;
    using System;
    using System.Threading.Tasks;

    public interface IUserGateway
    {
        Task<UserResponse> GetUserAsync(Guid userId);
    }
}
