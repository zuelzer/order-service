﻿namespace Checkout.OrderService.Domain.Model.Tests.Entities
{
    using Checkout.OrderService.Domain.Model.Entities;
    using Checkout.OrderService.Infrastructure.CrossCutting.Exceptions;
    using System;
    using Xunit;

    [Trait("Category", "Domain.Model")]
    public class OrderTests
    {
        [Fact]
        public void Constructor_ValidOrder_NewOrderWithFilledProperties()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var amount = 12;

            // Act
            var order = new Order(userId, amount);

            // Assert
            Assert.NotNull(order);
            Assert.Equal(userId, order.UserId);
            Assert.Equal(amount, order.Amount);
        }

        [Fact]
        public void SetUserName_ValidUserName_NamePropertyFilled()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var userName = "UserName";
            var amount = 12;

            // Act
            var order = new Order(userId, amount);
            order.SetUserName(userName);

            // Assert
            Assert.NotNull(order);
            Assert.Equal(userName, order.UserName);
        }

        [Fact]
        public void SetUserNameAndValidate_ValidOrderAndUserName_AllPropertiesFilled()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var userName = "UserName";
            var amount = 12;

            // Act
            var order = new Order(userId, amount);
            order.SetUserName(userName);
            order.Validate();

            // Assert
            Assert.NotNull(order);
            Assert.Equal(userId, order.UserId);
            Assert.Equal(userName, order.UserName);
            Assert.Equal(amount, order.Amount);
        }

        [Fact]
        public void Validate_NullUserId_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var userId = default(Guid);
            var amount = 12;
            var expectedMessage = "User id cannot be null";

            // Act
            var order = new Order(userId, amount);
            var exception = Assert.Throws<InvalidDomainEntityException>(() => order.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void Validate_NullUserName_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var amount = 12;
            var expectedMessage = "User name cannot be null";

            // Act
            var order = new Order(userId, amount);
            order.SetUserName(null);

            var exception = Assert.Throws<InvalidDomainEntityException>(() => order.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void Validate_UserNameLongerThanAHundredCharacters_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var userName = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tortor sed augue tortor.";
            var amount = 12;
            var expectedMessage = "User name cannot be longer than a hundred charactes";

            // Act
            var order = new Order(userId, amount);
            order.SetUserName(userName);

            var exception = Assert.Throws<InvalidDomainEntityException>(() => order.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void Validate_AmountEqualsZero_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var userName = "UserName";
            var amount = 0;
            var expectedMessage = "Amount must be greater than zero";

            // Act
            var order = new Order(userId, amount);
            order.SetUserName(userName);

            var exception = Assert.Throws<InvalidDomainEntityException>(() => order.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void Validate_AmountNegative_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var userId = Guid.NewGuid();
            var userName = "UserName";
            var amount = -1;
            var expectedMessage = "Amount must be greater than zero";

            // Act
            var order = new Order(userId, amount);
            order.SetUserName(userName);

            var exception = Assert.Throws<InvalidDomainEntityException>(() => order.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }
    }
}
